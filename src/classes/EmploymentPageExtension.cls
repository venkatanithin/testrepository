global class EmploymentPageExtension implements TF4SF.DSP_Interface {
	
	global Map<String, String> main(Map<String, String> tdata) {
		Long time1 = DateTime.now().getTime();
		Boolean infoDebug = false;
		Map<String, String> data = tdata.clone();
		Map<String, String> logs = new Map<String, String>();

		try {
			String appId = data.get('id');
			infoDebug = (tdata.get('infoDebug') == 'true');
			logs = SubmitToCore.JSONGenerator(data);
			LogStorage.InsertDebugLog(appId, logs.get('coreReq'), 'EmploymentPage Request');
			LogStorage.InsertDebugLog(appId, logs.get('coreRes'), 'EmploymentPage Response');
		} catch (Exception e) {
			data.put('server-errors', 'Error encountered in EmploymentPageExtension class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString());
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error encountered in EmploymentPageExtension class: ' + e.getMessage() + '; line: ' + e.getLineNumber() + '; type: ' + e.getTypeName() + '; stack trace: ' + e.getStackTraceString()));
			System.debug('server-errors: ' + e.getMessage());
		}

		Long time2 = DateTime.now().getTime();
		if (infoDebug == true ) { data.put('debug-server-errors', 'EmploymentPageExtension - Elapsed Call Time: ' + (time2 - time1) + 'ms'); }
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'EmploymentPageExtension - Elapsed Call Time: ' + (time2 - time1) + 'ms'));

		return null;
	}
}